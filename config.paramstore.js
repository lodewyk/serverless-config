/**
 * Gets the config from the AWS Parameter store using the provided key.
 */

const AWS = require('aws-sdk');
const ssm = new AWS.SSM();

module.exports = {
    getConfigValueByKey(key) {
        return new Promise((resolve, reject) => ssm.getParameter({ Name: key }, (err, data) => {
                if (err) {
                    console.log('An error occurred', err);
                    reject(err);
                    return;
                }
                resolve(JSON.parse(data.Parameter.Value));
            })
        );
    }
};

