/**
 * Gets the config from the AWS Secrets manager using the provided key.
 */

const AWS = require('aws-sdk');
const secretsManager = new AWS.SecretsManager();

module.exports = {
   getConfigValueByKey(key) {
       return new Promise((resolve, reject) => secretsManager.getSecretValue({SecretId: key}, (err, data) =>{
           if(err) {
               console.log('An error occured', err);
               reject(err);
               return;
            }
            resolve(JSON.parse(data.SecretString));
       }
    ))
   }  
};
