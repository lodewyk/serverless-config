/**
 * Single get that returns the config value. 
 * An environmental variable is used to check if we are running in the cloud or locally.
 */
const express = require('express');
const app = express();

const paramstoreOrLocal = process.env.IN_CLOUD ? require("./config.paramstore") :  require("./config.local");
const secretsOrConsul = process.env.IN_CLOUD ? require("./config.secrets") : require("./config.consul.local");

app.get('/', async (req, res) => {
    try {
        res.send(await paramstoreOrLocal.getConfigValueByKey("config"));
    } catch (e) {
        console.log(e);
        res.status(500).send('An exception occured...').end();
    }
});

app.get('/more', async (req, res) => {
    try {
        res.send(await secretsOrConsul.getConfigValueByKey("config"));
    } catch (e) {
        console.log(e);
        res.status(500).send('An exception occured...').end();
    }
});

module.exports = app;