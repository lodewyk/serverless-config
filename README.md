# serverless-config

## Running a local consul server

Ensure that you have docker-compose installed then run the following command from the project root:

`docker-compose up -d`

The consul web UI will be available at `localhost:8500`. 

The KV tab can be used to create configuration key value pairs. Create a KV pair with the key name `config` and the value as any valid JSON.

## Running the express NodeJS applicaiton locally

Ensure that you have NodeJS, and run the following command from the project root:

`node app.local.js`

The application will be available at `localhost:3000`. The `/` endpoint will read a configuration value from the `config.local.js` file, while the `/more` endpoint will return the config value from the Consul KV server.



## Running the application in AWS

### Creating a role with the correct permisssions

Create a Lambda executor role called `config-lambda-executor`and attach the following policies:

- `SecretsMananagerReadWrite`: This role allows the Lambda function to access the AWS Secrets manager service.
- `AmazonSSMReadOnlyAccess`: This role allows the Lambda function to access the AWS SSM (Systems Manager) Praramter store.

### Creating the configuration data in Parameter Store

Access the paramater store from the consul under the EC2 section.

Add a paramter with the key `config` and any valid JSON as the value.



### Creating the configuration data in AWS Secrets Manager

Access the AWS Secrets manager from the AWS console and create a secret (Using the Other secret type) with any key value pair. This translates into a JSON document as before. Name the secret `config`.



### Wrapping the express NodeJS application to be executed by Lambda

Ensure that you have claudia JS installed and run the following command from the project root to wrap the express NodeJS application in a Lambda function:

`claudia generate-serverless-express-proxy --express-module app`

This command will create a `lambda.js` file that Lambda will use to execute the application defined in the `app.js` file.

### Deploying the Lambda and creating the API gateway

Run the following command to create the API gateway and the Lambda in the specified region, settting an environmental variable and associating the correct role to the Lamba.

`claudia create --handler lambda.handler --deploy-proxy-api --region eu-west-1 --role config-lambda-executor --set-env IN_CLOUD=true --name config-lambda`

This command will use the credentials contained in your `~/.aws` folder to interact with AWS and should be set up using `aws configure` prior to running this command. Ensure that the used being used has the appropriate permissions to create the stack.

This command will also create a `claudia.json` file which claudia uses to keep state about the changes that were made and also gives a summary of the command that was executed.

The Lamda should now be available for invocation. Claudia prints out the endpoint in the console after deploying the Lambda and the API gateway.

Hitting the `.../latest` endpoint will read and return the config value from the Parameter store while the `.../latest/more` enpoint will return the value stored in the AWS Secrets manager.



### Making code changes

After making a code change and saving, the following command can be used to update the Lambda code and publish the changes:

`claudia update`










































































































































































































































































