/**
 * Simulates returning a promise from a local resource.
 */
module.exports = {
    getConfigValueByKey(key) {
        return new Promise((resolve, reject) => resolve({value: "This is a local config value"}));
    }
}
