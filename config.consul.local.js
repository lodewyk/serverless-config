/**
 * Simulates returning a promise from a local or on prem consul server.
 * 
 */
const consul = require('consul')({host: "localhost", port: 8500});

module.exports = {
    getConfigValueByKey(key) {
        return new Promise((resolve, reject) => consul.kv.get({key: key}, (err, result) => {
            if(err) {
                console.log('An error occured', err);
                reject(err);
                return;
            }
            resolve(JSON.parse(result.Value))
        }));
    }
};